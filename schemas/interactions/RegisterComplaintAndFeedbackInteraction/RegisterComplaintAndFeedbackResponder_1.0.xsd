<?xml version="1.0" encoding="UTF-8" ?>
<!-- 
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements. See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership. Inera AB licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License. You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied. See the License for the
 specific language governing permissions and limitations
 under the License.
 -->		
<xs:schema xmlns:xs='http://www.w3.org/2001/XMLSchema'
           xmlns:tns='urn:riv-application:supportprocess:logistics:complaintsandfeedback:RegisterComplaintAndFeedbackResponder:1'
           xmlns:core='urn:riv-application:supportprocess:logistics:complaintsandfeedback:1'
           targetNamespace='urn:riv-application:supportprocess:logistics:complaintsandfeedback:RegisterComplaintAndFeedbackResponder:1'
           elementFormDefault='qualified'
           attributeFormDefault='unqualified'
           version='1.0'>

  <xs:import schemaLocation='../../core_components/supportprocess_logistics_complaintsandfeedback_1.0.xsd' namespace='urn:riv-application:supportprocess:logistics:complaintsandfeedback:1' />

  <xs:element name='RegisterComplaintAndFeedback' type='tns:RegisterComplaintAndFeedbackType' />
  <xs:element name='RegisterComplaintAndFeedbackResponse' type='tns:RegisterComplaintAndFeedbackResponseType' />

  <!-- RegisterComplaintAndFeedbackType -->

  <xs:complexType name="RegisterComplaintAndFeedbackType">
    <xs:sequence>
      <xs:element name="complaintAndFeedback" type="tns:ComplaintAndFeedbackType" />
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
    </xs:sequence>
  </xs:complexType>

  <!-- Specific data types-->

  <xs:complexType name="AddressType">
    <xs:sequence>
      <xs:element name="careOf" type="xs:string" minOccurs="0" />
      <xs:element name="postalAddress1" type="xs:string" minOccurs="0" />
      <xs:element name="postalAddress2" type="xs:string" minOccurs="0" />
      <xs:element name="postalCode" type="xs:int" minOccurs="0" />
      <xs:element name="city" type="xs:string" minOccurs="0" />
      <xs:any maxOccurs="unbounded" minOccurs="0" namespace="##other" processContents="lax"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="AdministrativeGenderType">
    <xs:complexContent>
      <xs:restriction base="tns:CVType">
        <xs:sequence>
          <xs:element name="code" type="xs:string" />
          <xs:element name="codeSystem" type="xs:string" fixed="1.2.752.129.2.2.1.1" />
          <xs:element name="codeSystemName" type="xs:string" fixed="Kv kön" minOccurs="0" />
          <xs:element name="codeSystemVersion" type="xs:string" minOccurs="0" />
          <xs:element name="displayName" type="xs:string" minOccurs="0" />
          <xs:element name="originalText" type="xs:string" minOccurs="0" />
          <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
      </xs:restriction>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="RepresentativeType">
    <xs:sequence>
      <xs:element name="personId" type="tns:PersonIdType" />
      <xs:element name="name" type="core:HumanNameType" />
      <xs:element name="contactInformation" type="tns:ContactInformationType" minOccurs="0" />
      <xs:element name="type" type="tns:TypeType" />
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="ComplaintAndFeedbackType">
    <xs:sequence>
      <xs:element name="id" type="core:IIType" />
      <xs:element name="createTime" type="core:TimeStampType" />
      <xs:element name="event" type="tns:EventType" />
      <xs:element name="categorization" type="core:CategorizationType" minOccurs="0" />
      <xs:element name="improvementProposal" type="xs:string" minOccurs="0" />
      <xs:element name="positiveFeedback" type="xs:string" minOccurs="0" />
      <xs:element name="answerRequired" type="xs:boolean" />
      <xs:element name="question" type="xs:string" minOccurs="0" />
      <xs:element name="meansOfContact" type="tns:MeansOfContactType" minOccurs="0" />
      <xs:element name="consent" type="xs:boolean" minOccurs="0" />
      <xs:element name="receivingOrganizationUnit" type="tns:ReceivingOrganizationUnitType" />
      <xs:element name="representative" type="tns:RepresentativeType" minOccurs="0" />
      <xs:element name="patient" type="tns:PatientType" minOccurs="0" />
      <xs:element name="attachment" type="core:AttachmentType" minOccurs="0" maxOccurs="unbounded" />
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="ContactInformationType">
    <xs:sequence>
      <xs:element name="telephoneNumber" type="xs:string" minOccurs="0" />
      <xs:element name="address" type="tns:AddressType" minOccurs="0" />
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
    </xs:sequence>
  </xs:complexType>

  <xs:simpleType name="DateType">
    <xs:restriction base="xs:string">
      <xs:pattern value="(19|20)\d\d(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])" />
    </xs:restriction>
  </xs:simpleType>

  <xs:complexType name="EventType">
    <xs:sequence>
      <xs:element name="date" type="tns:DateType" />
      <xs:element name="description" type="xs:string" />
      <xs:element name="eventOrganizationUnit" type="core:OrgUnitType" />
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="MeansOfContactType">
    <xs:complexContent>
      <xs:restriction base="tns:CVType">
        <xs:sequence>
          <xs:element name="code" type="xs:string" />
          <xs:element name="codeSystem" type="xs:string" fixed="1.2.752.129.5.1.20" />
          <xs:element name="codeSystemName" type="xs:string" fixed="KV kontaktsätt" minOccurs="0" />
          <xs:element name="codeSystemVersion" type="xs:string" minOccurs="0" />
          <xs:element name="displayName" type="xs:string" minOccurs="0" />
          <xs:element name="originalText" type="xs:string" minOccurs="0" />
          <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
      </xs:restriction>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="PatientType">
    <xs:sequence>
      <xs:element name="personId" type="tns:PersonIdType" />
      <xs:element name="name" type="core:HumanNameType" />
      <xs:element name="contactInformation" type="tns:ContactInformationType" minOccurs="0" />
      <xs:element name="administrativeGender" type="tns:AdministrativeGenderType" />
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="PersonIdType">
    <xs:complexContent>
      <xs:restriction base="tns:IIType">
        <xs:sequence>
          <xs:element name="root" type="tns:PersonIdRootType" />
          <xs:element name="extension" type="xs:string" />
          <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
      </xs:restriction>
    </xs:complexContent>
  </xs:complexType>

  <xs:simpleType name="PersonIdRootType">
    <xs:restriction base="xs:string">
      <xs:enumeration value="1.2.752.129.2.1.3.1" />
    </xs:restriction>
  </xs:simpleType>

  <xs:complexType name="ReceivingOrganizationUnitType">
    <xs:complexContent>
      <xs:restriction base="tns:OrgUnitType">
        <xs:sequence>
          <xs:element name="orgUnitHSAId" type="tns:HSAIdType" />
          <xs:element name="orgUnitName" type="xs:string" minOccurs="0" />
          <xs:element name="orgUnitTelecom" type="xs:string" minOccurs="0" />
          <xs:element name="orgUnitEmail" type="xs:string" minOccurs="0" />
          <xs:element name="orgUnitAddress" type="xs:string" minOccurs="0" />
          <xs:element name="orgUnitLocation" type="xs:string" minOccurs="0" />
          <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
      </xs:restriction>
    </xs:complexContent>
  </xs:complexType>

  <xs:complexType name="TypeType">
    <xs:complexContent>
      <xs:restriction base="tns:CVType">
        <xs:sequence>
          <xs:element name="code" type="xs:string" />
          <xs:element name="codeSystem" type="xs:string" fixed="1.2.752.116.2.1.1" />
          <xs:element name="codeSystemName" type="xs:string" minOccurs="0" />
          <xs:element name="codeSystemVersion" type="xs:string" minOccurs="0" />
          <xs:element name="displayName" type="xs:string" minOccurs="0" />
          <xs:element name="originalText" type="xs:string" minOccurs="0" />
          <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
      </xs:restriction>
    </xs:complexContent>
  </xs:complexType>

  <!-- Ineras defacto CVType, IIType and OrgUnitType (defined here for complex types with restriction base CVType, IIType and OrgUnitType) -->

  <xs:complexType name="CVType">
    <xs:sequence>
      <xs:element name="code" type="xs:string"/>
      <xs:element name="codeSystem" type="xs:string"/>
      <xs:element name="codeSystemName" type="xs:string" minOccurs="0"/>
      <xs:element name="codeSystemVersion" type="xs:string" minOccurs="0"/>
      <xs:element name="displayName" type="xs:string" minOccurs="0"/>
      <xs:element name="originalText" type="xs:string" minOccurs="0"/>
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
    </xs:sequence>
  </xs:complexType>

  <xs:simpleType name="HSAIdType">
    <xs:restriction base="xs:string"/>
  </xs:simpleType>

  <xs:complexType name="IIType">
    <xs:sequence>
      <xs:element name="root" type="xs:string"/>
      <xs:element name="extension" type="xs:string" minOccurs="0"/>
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="OrgUnitType">
    <xs:sequence>
      <xs:element name="orgUnitHSAId" type="tns:HSAIdType" minOccurs="0" />
      <xs:element name="orgUnitName" type="xs:string" minOccurs="0" />
      <xs:element name="orgUnitTelecom" type="xs:string" minOccurs="0" />
      <xs:element name="orgUnitEmail" type="xs:string" minOccurs="0" />
      <xs:element name="orgUnitAddress" type="xs:string" minOccurs="0" />
      <xs:element name="orgUnitLocation" type="xs:string" minOccurs="0" />
      <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
    </xs:sequence>
  </xs:complexType>

  <!-- RegisterComplaintAndFeedbackResponseType -->

  <xs:complexType name='RegisterComplaintAndFeedbackResponseType'>
    <xs:sequence>
      <xs:element name='resultCode' type='tns:ResultCodeEnum' minOccurs='1' maxOccurs='1' />
      <xs:element name='resultText' type='xs:string' minOccurs='0' maxOccurs='1' />
      <xs:any namespace='##other' processContents='lax' minOccurs='0' maxOccurs='unbounded' />
    </xs:sequence>
  </xs:complexType>

  <xs:simpleType name='ResultCodeEnum'>
    <xs:restriction base='xs:string'>
      <xs:enumeration value='OK' />
      <xs:enumeration value='ERROR' />
      <xs:enumeration value='INFO' />
    </xs:restriction>
  </xs:simpleType>

</xs:schema>