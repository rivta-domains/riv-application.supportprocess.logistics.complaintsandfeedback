<?xml version="1.0" encoding="UTF-8" ?>
<!--
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements. See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership. Inera AB licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License. You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied. See the License for the
 specific language governing permissions and limitations
 under the License.
 -->
<xs:schema xmlns:xs='http://www.w3.org/2001/XMLSchema'
           xmlns:tns='urn:riv-application:supportprocess:logistics:complaintsandfeedback:RegisterMessageResponder:1'
           xmlns:core='urn:riv-application:supportprocess:logistics:complaintsandfeedback:1'
           targetNamespace='urn:riv-application:supportprocess:logistics:complaintsandfeedback:RegisterMessageResponder:1'
           elementFormDefault='qualified'
           attributeFormDefault='unqualified'
           version='1.0'>

    <xs:import schemaLocation='../../core_components/supportprocess_logistics_complaintsandfeedback_1.0.xsd' namespace='urn:riv-application:supportprocess:logistics:complaintsandfeedback:1' />

    <xs:element name='RegisterMessage' type='tns:RegisterMessageType' />
    <xs:element name='RegisterMessageResponse' type='tns:RegisterMessageResponseType' />

    <!-- RegisterMessageType -->

    <xs:complexType name="RegisterMessageType">
        <xs:sequence>
            <xs:element name="message" type="tns:MessageType" />
            <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
    </xs:complexType>

    <!-- Specific data types-->

    <xs:complexType name="AuthorOrganizationUnitType">
        <xs:complexContent>
            <xs:restriction base="tns:OrgUnitType">
                <xs:sequence>
                    <xs:element name="orgUnitHSAId" type="tns:HSAIdType" />
                    <xs:element name="orgUnitName" type="xs:string" minOccurs="0" />
                    <xs:element name="orgUnitTelecom" type="xs:string" minOccurs="0" />
                    <xs:element name="orgUnitEmail" type="xs:string" minOccurs="0" />
                    <xs:element name="orgUnitAddress" type="xs:string" minOccurs="0" />
                    <xs:element name="orgUnitLocation" type="xs:string" minOccurs="0" />
                    <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
                </xs:sequence>
            </xs:restriction>
        </xs:complexContent>
    </xs:complexType>

    <xs:complexType name="AuthorType">
        <xs:sequence>
            <xs:element name="name" type="core:HumanNameType" />
            <xs:element name="authorOrganizationUnit" type="tns:AuthorOrganizationUnitType" minOccurs="0" />
            <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
    </xs:complexType>


    <xs:complexType name="MessageType">
        <xs:sequence>
            <xs:element name="id" type="core:IIType" />
            <xs:element name="createTime" type="core:TimeStampType" />
            <xs:element name="type" type="tns:MessageTypeType" />
            <xs:element name="referenceId" type="core:IIType" />
            <xs:element name="content" type="core:RestrictedStringType" />
            <xs:element name="referencedMessageId" type="core:IIType" minOccurs="0" />
            <xs:element name="attachment" type="core:AttachmentType" minOccurs="0" maxOccurs="unbounded" />
            <xs:element name="author" type="tns:AuthorType" />
            <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="MessageTypeType">
        <xs:complexContent>
            <xs:restriction base="tns:CVType">
                <xs:sequence>
                    <xs:element name="code" type="xs:string"/>
                    <xs:element name="codeSystem" type="xs:string" fixed="1.2.752.129.5.1.31" />
                    <xs:element name="codeSystemName" type="xs:string" fixed="KV meddelandetyp" minOccurs="0"/>
                    <xs:element name="codeSystemVersion" type="xs:string" minOccurs="0"/>
                    <xs:element name="displayName" type="xs:string" minOccurs="0"/>
                    <xs:element name="originalText" type="xs:string" minOccurs="0"/>
                    <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
                </xs:sequence>
            </xs:restriction>
        </xs:complexContent>
    </xs:complexType>

    <!-- Ineras defacto CVType, HSAIdType and OrgUnitType (defined here for complex types with restriction base CVType and OrgUnitType) -->

    <xs:complexType name="CVType">
        <xs:sequence>
            <xs:element name="code" type="xs:string"/>
            <xs:element name="codeSystem" type="xs:string"/>
            <xs:element name="codeSystemName" type="xs:string" minOccurs="0"/>
            <xs:element name="codeSystemVersion" type="xs:string" minOccurs="0"/>
            <xs:element name="displayName" type="xs:string" minOccurs="0"/>
            <xs:element name="originalText" type="xs:string" minOccurs="0"/>
            <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
    </xs:complexType>

    <xs:simpleType name="HSAIdType">
        <xs:restriction base="xs:string"/>
    </xs:simpleType>

    <xs:complexType name="OrgUnitType">
        <xs:sequence>
            <xs:element name="orgUnitHSAId" type="tns:HSAIdType" minOccurs="0" />
            <xs:element name="orgUnitName" type="xs:string" minOccurs="0" />
            <xs:element name="orgUnitTelecom" type="xs:string" minOccurs="0" />
            <xs:element name="orgUnitEmail" type="xs:string" minOccurs="0" />
            <xs:element name="orgUnitAddress" type="xs:string" minOccurs="0" />
            <xs:element name="orgUnitLocation" type="xs:string" minOccurs="0" />
            <xs:any namespace="##other" processContents="lax" minOccurs="0" maxOccurs="unbounded" />
        </xs:sequence>
    </xs:complexType>

    <!-- RegisterMessageResponseType -->

    <xs:complexType name='RegisterMessageResponseType'>
        <xs:sequence>
            <xs:element name='resultCode' type='tns:ResultCodeEnum' minOccurs='1' maxOccurs='1' />
            <xs:element name='resultText' type='xs:string' minOccurs='0' maxOccurs='1' />
            <xs:any namespace='##other' processContents='lax' minOccurs='0' maxOccurs='unbounded' />
        </xs:sequence>
    </xs:complexType>

    <xs:simpleType name='ResultCodeEnum'>
        <xs:restriction base='xs:string'>
            <xs:enumeration value='OK' />
            <xs:enumeration value='ERROR' />
            <xs:enumeration value='INFO' />
        </xs:restriction>
    </xs:simpleType>

</xs:schema>